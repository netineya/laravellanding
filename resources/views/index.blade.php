@include('blocks.head.index')
<body class="bg-secondary text-white">
@include('blocks.navbar.index')
<div class="container bg-secondary">

@yield('new_content')

</div>
<script src="{{ asset('js/app.js') }}"></script>
{{--<script>
    $(function() {
        $('.alert-success').fadeOut(3000);
    });

    $('.navbar-nav>li>a').on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });

</script>--}}
</body>

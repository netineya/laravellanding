@extends('index')
@section('title')
    Контакты
@endsection
@section('new_content')
    <div class="m-3 p-3 bg-dark dop-div">
    <p><a class="btn btn-success table-buttons" href="mailto:netineya@yandex.ru">Написать на email</a></p>
    <p>
        <a href="https://vk.com/orlov_i_v" class="btn btn-success table-buttons" title="написать мне в ВК" target="_blank">
            <i class="fa fa-vk" aria-hidden="true" style=""></i>
        </a>
    </p>
    </div>
@endsection

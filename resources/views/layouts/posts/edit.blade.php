@extends('index')


@section('title')
{{ $post->title }}
@endsection


@section('new_content')
    <div class="m-3 p-3 bg-dark dop-div">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('posts.update', $post) }}">
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="post-title">Название</label>
            <input type="text" name="title"
                   value="{{$post->title}}" class="form-control" id="post-title">
        </div>
        <div class="form-group">
            <label for="post-description">Описание</label>
            <textarea name="description" class="form-control" id="post-description" rows="3">{{$post->description}}</textarea>
        </div>
      {{--  <div class="form-group">
            <label for="post-price">Цена</label>
            <input type="text" name="price"
                   value="{{$post->price}}"$ class="form-control" id="post-price">
        </div>  --}}
        <button type="submit" class="btn btn-success">Редактировать пост</button>
    </form>
    </div>
@endsection

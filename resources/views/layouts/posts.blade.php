@extends('index')


@section('title')
 Посты
@endsection


@section('new_content')
    <div class="m-3 p-3 bg-dark dop-div">
    <a href="{{ route('posts.create') }}" class="btn btn-success">Создать пост</a>
    </div>
    @if(session()->get('success'))
        <div class="alert alert-success mt-3">
            {{ session()->get('success') }}
        </div>
    @endif



                @foreach($posts as $post)
                    <div class="m-3 p-3 bg-dark dop-div">
                <table class="table table-striped table-dark mt-3">
                        <tr><td class="dop-td">{{$post->title}}</td></tr>
                <tr><td><pre class="table-pre">{{$post->description}}</pre></td></tr>
                <tr><td class="table-buttons">
                    <a href="{{ route('posts.show',$post) }}" class="btn btn-success" title="посмотреть">
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="{{ route('posts.edit',$post) }}" class="btn btn-primary" title="отредактировать">
                        <i class="fa fa-pencil" ></i>
                    </a>
                    <form method="POST" action="{{ route('posts.destroy',$post) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" title="удалить">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                    </td></tr>
                </table>
                    </div>
            @endforeach

@endsection

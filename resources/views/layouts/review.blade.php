@extends('index')
@section('title')
    Отзывы
@endsection
@section('new_content')
    <div class="m-3 p-3 bg-dark dop-div">

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif

    <h1>Форма добавления отзыва</h1>
    <form method="post" action="/review/check">
        @csrf
        <input type="email" name="email" value="{{old('email')}}" id="email" placeholder="Введите email" class="form-control"><br>
        <input type="text" name="subject" value="{{old('subject')}}" id="subject" placeholder="Введите отзыв" class="form-control"><br>
        <textarea name="message" id="message" class="form-control" placeholder="Введите сообщение">{{old('message')}}</textarea><br>
        <button type="submit" class="btn btn-success">Отправить</button>
    </form>
    </div>
    @if(session()->get('success'))
        <div class="alert alert-success mt-3">
            {{ session()->get('success') }}
        </div>
    @endif
    <div class="m-3 p-3 bg-dark dop-div">
    <br>
    <h1>Все отзывы:</h1>
    @foreach($reviews as $el)
        <div class="alert alert-warning">
            <h3>{{$el->subject}}</h3>
            <b>{{$el->email}}</b>
            <p>{{$el->message}}</p>
            <p>{{$el->date}}</p>
        </div>
    @endforeach
    </div>
@endsection

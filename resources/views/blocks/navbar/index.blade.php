<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">Blog начинающего разработчика</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        @php(
        $links = ([
            '/'=>'blog',
            'tables'=>'Запросы в таблицу',
            'contacts'=>'Контакты',
            'posts'=>'Статьи',
            'review'=>'Отзывы',
]))



        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @foreach($links as $link => $value )
                    @if((Request::path()) == $link)
                        <li class = "nav-item">
                            <a class="nav-link active" href="{{url($link)}}">{{$value}}<span class="sr-only">(current)</span></a>
                        </li>
                    @elseif((Request::path()) != $link)
                        <li class = "nav-item">
                            <a class="nav-link" href="{{url($link)}}">{{$value}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</nav>

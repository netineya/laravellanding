<?php

use App\Http\Controllers\NavLinkController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PostController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', [MainController::class,'blog']);
Route::get('/', [MainController::class,'blog']);
Route::get('/tables', [MainController::class,'tables']);
Route::get('/contacts',[MainController::class,'contacts']);
//Route::get('/posts', [MainController::class,'posts']);
Route::get('/review',[MainController::class,'review'])->name('review2');
Route::post('/review/check',[MainController::class,'review_check']);
Route::resource('/posts',PostController::class);
Route::get('/links',[NavLinkController::class,'mainMenu']);

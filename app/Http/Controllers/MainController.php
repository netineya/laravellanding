<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class MainController extends Controller
{
    public function blog()
    {
        return view('layouts.blog');
    }

    public function tables()
    {
        return view('layouts.tables');
    }

    public function contacts()
    {
        return view('layouts.contacts');
    }



    public function review(){
        $reviews = new Contact();
        //dd($reviews);
        return view('layouts.review',['reviews' => $reviews->all()]);
    }

    public function review_check(Request $request){


       $valid = $request->validate([
            'email' => 'required|min:4|max:100',
            'subject' => 'required|min:4|max:100',
            'message' => 'required|min:15|max:500',
        ]);

        $review = new Contact();
        $review->email = $request->input('email');
        $review->subject = $request->input('subject');
        $review->message = $request->input('message');

        $review->save();

        return redirect()->route('review2')->with('success','Отзыв успешно добавлен');
    }

}

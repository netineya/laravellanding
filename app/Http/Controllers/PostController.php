<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $posts = Post::all();

        return view('layouts.posts',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('layouts.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|max:255',
            'description'=>'required|max:10000',
          //  'price'=>'required|numeric',
        ]);

        $post = new Post([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        //    'price' => $request->get('price'),
        ]);

        $post->save();

        return redirect('/posts')->with('success','Пост успешно добавлен');
//return dd($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('layouts.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return Response
     */
    public function edit($id)
    {

        $post = Post::find($id);
        return view('layouts.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required|max:255',
            'description'=>'required|max:10000',
           // 'price'=>'required|numeric',
        ]);

        $post = Post::find($id);
       $post->title = $request->get('title');
      $post->description = $request->get('description');
      //  $post->price = $request->get('price');

   //     $post = new Post([
    //        'title' => $request->get('title'),
    //        'description' => $request->get('description'),
    //        'price' => $request->get('price'),
    //    ]);

        $post->save();

        return redirect('/posts')->with('success','Пост успешно отредактирован');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return Response
     */
    public function destroy($id)

    {
        $post = Post::find($id);
        $post->delete();

        return redirect('/posts')->with('success','Пост успешно удален');
    }
}
